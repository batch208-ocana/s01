package com.b208.s1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.HashMap;

@SpringBootApplication
@RestController
//It will tell springboot that this application will function as an endpoint handling web request
public class S1Application {

	public static void main(String[] args) {
		//Annotations in java Springboot marks classes for their intended functionalities
		//Springboot upon startup scans for classes and assign behaviors based on their annotation
		SpringApplication.run(S1Application.class, args);
	}

	@GetMapping("/hello")
	//@GetMapping will map a route or an endpoint to access or run our method.
	//When http://localhost:8080/hello is accessed from a client, we will be able to rung the hi() method
	public String hello(){
		return "Hi from our Java Springboot App!";
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value="name",defaultValue = "John")
					 String nameParam){
		//Pass data through the url using our string query
		//http://localhost:8080/hi?name=Joe
		//We retrieve the value of the field name from our url
		//defaultValue is the fallback value when the query string or request param is empty
		return "Hi! My name is " +nameParam;
	}

	//2 ways of passing data through the URL by using JAVA Springboot.
	//Query string using request params - Directly passing data into the url and getting the data
	//path variable is much more similar to expressJS req.params

	@GetMapping("/favefood")
	public String myFavoriteFood(@RequestParam(value="food",defaultValue = "Chocolate") String foodParam){
		return "Hi my favorite food is " +foodParam;
	}

	ArrayList<String> enrollees = new ArrayList<>();

	@GetMapping("/enroll")
	public String enroll(@RequestParam(value = "user", defaultValue = "Chaz")
						 String userParam){
		enrollees.add(userParam);
		return "Welcome, " +userParam +"!";
	}

	@GetMapping("/getEnrollees")
	public ArrayList getEnrollees(){
		return enrollees;
	}

	@GetMapping("/courses/{id}")
	public String getCourses(@PathVariable String id){
		switch (id){
			case "java101":
				return "Name: Java 101, Schedule: MWF 8:00 AM - 11:00 AM, PHP 3000";
			case "sql101":
				return "Name: SQL 101, Schedule: MWF 8:00 AM - 11:00 AM, PHP 3000";
			case "jsoop101":
				return "Name: JSOOP 101, Schedule: MWF 8:00 AM - 11:00 AM, PHP 3000";
			default:
				return "Course cannot be found";
		}
	}
}
